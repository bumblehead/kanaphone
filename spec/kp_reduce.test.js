import test from 'node:test'
import assert from 'node:assert/strict'
import url from 'url'
import fs from 'fs'
import path from 'path'
import kp from '../src/kp.js'

const __filename = new url.URL('', import.meta.url).pathname
const DIRNAME = __filename.replace(/[/\\][^/\\]*?$/, '')

const gizadeathstarPath = path.join(DIRNAME, './gizadeathstar.md')

// `<kana title="${phoneme}">${kana}</kana>`
test('kp.reduce breaks a string to an array of strings', () => {
  const localarr = []
  const newarr = kp.reduce(kp.cfg({
    chardistmax : 3
  }), 'quintessentially yours', ((prev, substr, [, kana]) => {
    const val = kana
      ? `<kana title="${substr}">${kana}</kana>`
      : substr

    localarr.push(val)
    prev.push(val)

    return prev
  }), [])

  assert.deepEqual(localarr, newarr)
})

// `<kana title="${phoneme}">${kana}</kana>`
test('kp.reduce breaks a larger string to an array of strings', () => {
  fs.readFile(gizadeathstarPath, 'utf-8', (err, content) => {
    const localarr = []
    const newarr = kp.reduce(kp.cfg({
      chardistmax : 3
    }), content, ((prev, substr, [, kana]) => {
      const val = kana
        ? `<kana title="${substr}">${kana}</kana>`
        : substr

      localarr.push(val)
      prev.push(val)

      return prev
    }), [])

    console.dir(newarr, { maxArrayLength : null })
    assert.deepEqual(localarr, newarr)
  })
})
