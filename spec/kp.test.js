// Filename: kp.spec.js
// Timestamp: 2018.01.14-13:30:44 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'
import url from 'url'
import fs from 'fs'
import path from 'path'
import kp from '../src/kp.js'

const __filename = new url.URL('', import.meta.url).pathname
const DIRNAME = __filename.replace(/[/\\][^/\\]*?$/, '')

const loremipsumPath = path.join(DIRNAME, './loremipsum.md')

const getiscjkre = () => new RegExp([
  '[',
  '\u3000-\u303f', // Punctuation
  '\u3040-\u309f', // Hiragana
  '\u30a0-\u30ff', // Katakana
  '\uff00-\uff9f', // Full-Width Roman, Half-Width Katakana
  '\u4e00-\u9faf', // CJK (Common and Uncommon)
  '\u3400-\u4dbf', // CJK Ext. A (rare)
  ']'
].join(''), 'g')

// quintessential
test('kp should return a new string w/ multiple kana', () => {
  const iscjkre = getiscjkre()
  const [, newstr] = kp('quintessential')

  assert.strictEqual(iscjkre.test(newstr), true)
})

// `<kana title="${phoneme}">${kana}</kana>`
test('kp should return new string w/ multiple kana element strings', () => {
  const iscjkre = getiscjkre()
  const [, newstr] = kp('quintessential', kp.cfg({
    chardistmax : 3,
    replace : (word, substr, [, kana, phoneme]) => (
      `<kana title="${phoneme}">${kana}</kana>`)
  }))

  assert.strictEqual(iscjkre.test(newstr), true)
})

test('kp should return a new string w/ kana', () => {
  fs.readFile(loremipsumPath, 'utf-8', (err, content) => {
    // console.log('old', content)
    // console.log('new', kp(content)[1])
    assert.strictEqual(typeof content, 'string')
    assert.strictEqual(err, null)
  })
})
