// Filename: kp_queue.spec.js
// Timestamp: 2018.01.14-13:17:23 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'
import kp from '../src/kp.js'

test('kp.queue should manage a queue', () => {
  let queue = kp.queue([1, 2, 3], 0),
      value = kp.queue.qvalue(queue)

  assert.strictEqual(value, 2)

  queue = kp.queue.rm(queue)
  value = kp.queue.qvalue(queue)

  assert.strictEqual(value, 3)

  queue = kp.queue.rm(queue)
  value = kp.queue.qvalue(queue)

  assert.strictEqual(value, 1)

  queue = kp.queue.rm(queue)
  value = kp.queue.qvalue(queue)

  assert.strictEqual(value, undefined)
})
