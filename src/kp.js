// Filename: kp.js
// Timestamp: 2018.01.11-20:34:24 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import kp_replace from './kp_replace.js'
import kp_reduce from './kp_reduce.js'
import kp_queue from './kp_queue.js'
import kp_hiragana from './kp_hiragana.js'
import kp_katakana from './kp_katakana.js'
import kp_cfg from './kp_cfg.js'

export default Object.assign((str, cfg = kp_cfg.get()) => kp_replace(str, cfg), {
  queue: kp_queue,
  hiragana: kp_hiragana,
  katakana: kp_katakana,
  replace: kp_replace,
  reduce: kp_reduce,
  cfg: kp_cfg
})
