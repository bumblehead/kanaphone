// Filename: kp_replace.js
// Timestamp: 2018.01.14-00:58:48 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import kp_cfg from './kp_cfg.js'
import kp_matchdef from './kp_matchdef.js'

const replacesubmatch = (cfg, word, wordgr) => {
  const subwordoffset = +cfg.chardist
  let subwordgr

  if (wordgr.length > cfg.chardist) {
    subwordgr = wordgr.slice(subwordoffset)
    cfg = kp_cfg.removechardist(cfg, wordgr.length)

    if (subwordgr.length) {
      [cfg, subwordgr] = replacewordmatches(cfg, word, subwordgr)

      wordgr = wordgr.slice(0, subwordoffset) + subwordgr
    }
  } else {
    cfg = kp_cfg.removechardist(cfg, wordgr.length)
  }

  return [cfg, wordgr]
}

const replacewordmatches = (cfg, word, wordgr) => {
  let matchdef
  let matchoffset

  // if its not replacing... remove char dist
  if (!kp_cfg.isreplacing(cfg)) {
    [cfg, wordgr] = replacesubmatch(cfg, word, wordgr)
  }

  if (kp_cfg.isreplacing(cfg)) {
    matchdef = kp_matchdef(wordgr, cfg)
  }

  if (matchdef) {
    // offset
    // The offset of the matched substring within the whole string
    // being examined. (For example, if the whole string was 'abcd',
    // and the matched substring was 'bc', then this argument will
    // be 1.)
    wordgr = wordgr.replace(matchdef[3], (match, offset) => {
      cfg = kp_cfg.resetchardist(cfg)

      const replacestr = cfg.replace(wordgr, match, matchdef)

      matchoffset = offset + replacestr.length

      return replacestr
    })

    if (typeof matchoffset === 'number') {
      // subwordgr = wordgr.slice(matchoffset)
      [cfg, word] = replacesubmatch(
        cfg, word, wordgr.slice(matchoffset))

      wordgr = wordgr.slice(0, matchoffset) + word

      cfg = kp_cfg.setlastworddist(cfg, wordgr.length - 1)
    }
  }

  return [cfg, wordgr]
}

const replacetext = (str, cfg) => {
  str = str.replace(/([^\s-]*)/gi, (wordmatch, wordgr) => {
    [cfg, wordgr] = replacewordmatches(cfg, wordmatch, wordgr)

    return wordgr
  })

  return [cfg, str]
}

export default replacetext
