// Filename: kp_hiragana.js
// Timestamp: 2018.01.13-23:26:38 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import kp_matchlist from './kp_matchlist.js'

// eslint-disable-next-line one-var
const あ = 'あ', // a
      い = 'い', // i
      う = 'う', // u
      え = 'え', // e
      お = 'お', // o

      か = 'か', // ka
      き = 'き', // ki
      く = 'く', // ku
      け = 'け', // ke
      こ = 'こ', // ko

      さ = 'さ', // sa
      し = 'し', // shi
      す = 'す', // su
      せ = 'せ', // se
      そ = 'そ', // so

      た = 'た', // ta
      ち = 'ち', // chi
      つ = 'つ', // tsu
      て = 'て', // te
      と = 'と', // to

      な = 'な', // na
      に = 'に', // ni
      ぬ = 'ぬ', // nu
      ね = 'ね', // ne
      の = 'の', // no

      は = 'は', // ha
      ひ = 'ひ', // hi
      ふ = 'ふ', // fu
      へ = 'へ', // he
      ほ = 'ほ', // ho

      ま = 'ま', // ma
      み = 'み', // mi
      む = 'む', // mu
      め = 'め', // me
      も = 'も', // mo

      や = 'や', // ya
      ゆ = 'ゆ', // yu
      よ = 'よ', // yo

      ら = 'ら', // ra
      り = 'り', // ri
      る = 'る', // ru
      れ = 'れ', // re
      ろ = 'ろ', // ro

      わ = 'わ', // wa
      を = 'を', // wo
      ん = 'ん', // n

      が = 'が', // ga
      ざ = 'ざ', // za
      だ = 'だ', // da
      ば = 'ば', // ba
      ぱ = 'ぱ', // pa

      ぎ = 'ぎ', // gi
      じ = 'じ', // ji
      ぢ = 'ぢ', // ji
      び = 'び', // bi
      ぴ = 'ぴ', // pi

      ぐ = 'ぐ', // gu
      ず = 'ず', // zu
      づ = 'づ', // zu
      ぶ = 'ぶ', // bu
      ぷ = 'ぷ', // pu

      げ = 'げ', // ge
      ぜ = 'ぜ', // ze
      で = 'で', // de
      べ = 'べ', // be
      ぺ = 'ぺ', // pe

      ご = 'ご', // go
      ぞ = 'ぞ', // zo
      ど = 'ど', // do
      ぼ = 'ぼ', // bo
      ぽ = 'ぽ', // po

      きゃ = 'きゃ', // kya
      しゃ = 'しゃ', // sha
      ちゃ = 'ちゃ', // cha
      ひゃ = 'ひゃ', // hya
      ぴゃ = 'ぴゃ', // pya

      きゅ = 'きゅ', // kyu
      しゅ = 'しゅ', // shu
      ちゅ = 'ちゅ', // chu
      ひゅ = 'ひゅ', // hyu
      ぴゅ = 'ぴゅ', // pyu

      きょ = 'きょ', // kyo
      しょ = 'しょ', // sho
      ちょ = 'ちょ', // cho
      ひょ = 'ひょ', // hyo
      ぴょ = 'ぴょ', // pyo

      ぎゃ = 'ぎゃ', // gya
      じゃ = 'じゃ', // ja
      にゃ = 'にゃ', // nya
      びゃ = 'びゃ', // bya
      みゃ = 'みゃ', // mya

      ぎゅ = 'ぎゅ', // gya
      じゅ = 'じゅ', // ju
      にゅ = 'にゅ', // nyu
      びゅ = 'びゅ', // byu
      みゅ = 'みゅ', // my

      ぎょ = 'ぎょ', // gyo
      じょ = 'じょ', // jo
      にょ = 'にょ', // nyo
      びょ = 'びょ', // byo
      みょ = 'みょ', // myo

      りゃ = 'りゃ', // rya
      りゅ = 'りゅ', // ryu
      りょ = 'りょ', // ryu
      ぢゃ = 'ぢゃ', // (ja)
      ぢゅ = 'ぢゅ', // (ju)


      phoneme = {
        あ : ['a'],
        い : ['i'],
        う : ['u'],
        え : ['e'],
        お : ['o'],

        か : ['ka'],
        き : ['ki'],
        く : ['ku'],
        け : ['ke'],
        こ : ['ko'],

        さ : ['sa'],
        し : ['shi'],
        す : ['su'],
        せ : ['se'],
        そ : ['so'],

        た : ['ta'],
        ち : ['chi'],
        つ : ['tsu', 'tu'],
        て : ['te'],
        と : ['to'],

        な : ['na'],
        に : ['ni'],
        ぬ : ['nu'],
        ね : ['ne'],
        の : ['no'],

        は : ['ha'],
        ひ : ['hi'],
        ふ : ['fu'],
        へ : ['he'],
        ほ : ['ho'],

        ま : ['ma'],
        み : ['mi'],
        む : ['mu'],
        め : ['me'],
        も : ['mo'],

        や : ['ya'],
        ゆ : ['yu'],
        よ : ['yo'],

        ら : ['ra'],
        り : ['ri'],
        る : ['ru'],
        れ : ['re'],
        ろ : ['ro'],

        わ : ['wa'],
        を : ['wo'],
        ん : ['n'],

        が : ['ga'],
        ざ : ['za'],
        だ : ['da'],
        ば : ['ba'],
        ぱ : ['pa'],

        ぎ : ['gi'],
        じ : ['ji'],
        ぢ : ['ji'],
        び : ['bi'],
        ぴ : ['pi'],

        ぐ : ['gu'],
        ず : ['zu'],
        づ : ['zu'],
        ぶ : ['bu'],
        ぷ : ['pu'],

        げ : ['ge'],
        ぜ : ['ze'],
        で : ['de'],
        べ : ['be'],
        ぺ : ['pe'],

        ご : ['go'],
        ぞ : ['zo'],
        ど : ['do'],
        ぼ : ['bo'],
        ぽ : ['po'],

        きゃ : ['kya'],
        しゃ : ['sha'],
        ちゃ : ['cha'],
        ひゃ : ['hya'],
        ぴゃ : ['pya'],

        きゅ : ['kyu'],
        しゅ : ['shu'],
        ちゅ : ['chu'],
        ひゅ : ['hyu'],
        ぴゅ : ['pyu'],

        きょ : ['kyo'],
        しょ : ['sho'],
        ちょ : ['cho'],
        ひょ : ['hyo'],
        ぴょ : ['pyo'],

        ぎゃ : ['gya'],
        じゃ : ['ja'],
        にゃ : ['nya'],
        びゃ : ['bya'],
        みゃ : ['mya'],

        ぎゅ : ['gya'],
        じゅ : ['ju'],
        にゅ : ['nyu'],
        びゅ : ['byu'],
        みゅ : ['my'],

        ぎょ : ['gyo'],
        じょ : ['jo'],
        にょ : ['nyo'],
        びょ : ['byo'],
        みょ : ['myo'],

        りゃ : ['rya'],
        りゅ : ['ryu'],
        りょ : ['ryu'],
        ぢゃ : ['ja'],
        ぢゅ : ['ju']
      },

      // [regexp, char, group, [phone1, phone2]]
      /* eslint-disable object-property-newline */
      kana = {
        // a, i, u, e, o
        あ, い, う, え, お,
        // ka, ki, ku, ke, ko
        か, き, く, け, こ,
        // sa, shi, su, se, so
        さ, し, す, せ, そ,
        // ta, chi, tsu, te, to
        た, ち, つ, て, と,
        // na, ni, nu, ne, no
        な, に, ぬ, ね, の,
        // ha, hi, fu, he, ho
        は, ひ, ふ, へ, ほ,
        // ma, mi, mu, me, mo
        ま, み, む, め, も,
        // ya, yu, yo
        や, ゆ, よ,
        // ra, ri, ru, re, ro
        ら, り, る, れ, ろ,
        // wa, wo, n
        わ, を, ん,
        // ga, za, da, ba, pa
        が, ざ, だ, ば, ぱ,
        // gi, ji, ji, bi, pi
        ぎ, じ, ぢ, び, ぴ,
        // gu, zu, zu, bu, pu
        ぐ, ず, づ, ぶ, ぷ,
        // ge, ze, de, be, pe
        げ, ぜ, で, べ, ぺ,
        // go, zo, do, bo, po
        ご, ぞ, ど, ぼ, ぽ,
        // kya, sha, cha, hya, pya
        きゃ, しゃ, ちゃ, ひゃ, ぴゃ,
        // kyu, shu, chu, hyu, pyu
        きゅ, しゅ, ちゅ, ひゅ, ぴゅ,
        // kyo, sho, cho, hyo, pyo
        きょ, しょ, ちょ, ひょ, ぴょ,
        // gya, ja, nya, bya, mya
        ぎゃ, じゃ, にゃ, びゃ, みゃ,
        // gya, ju, nyu, byu, my
        ぎゅ, じゅ, にゅ, びゅ, みゅ,
        // gyo, jo, nyo, byo, myo
        ぎょ, じょ, にょ, びょ, みょ,
        // rya, ryu, ryu, (ja), (ju)
        りゃ, りゅ, りょ, ぢゃ, ぢゅ
      },

      kanagroups = [
        // a, i, u, e, o,  n (added)
        [あ, [あ, い, う, え, お, ん]],
        // ka, ki, ku, ke, ko
        [か, [か, き, く, け, こ]],
        // sa, shi, su, se, so
        [さ, [さ, し, す, せ, そ]],
        // ta, chi, tsu, te, to
        [た, [た, ち, つ, て, と]],
        // na, ni, nu, ne, no
        [な, [な, に, ぬ, ね, の]],
        // ha, hi, fu, he, ho
        [は, [は, ひ, ふ, へ, ほ]],
        // ma, mi, mu, me, mo
        [ま, [ま, み, む, め, も]],
        // ya, yu, yo
        [や, [や, ゆ, よ]],
        // ra, ri, ru, re, ro
        [ら, [ら, り, る, れ, ろ]],
        // wa, wo, n  (removed)
        [わ, [わ, を/* , ん */]],
        // ga, za, da, ba, pa
        [が, [が, ざ, だ, ば, ぱ]],
        // gi, ji, ji, bi, pi
        [ぎ, [ぎ, じ, ぢ, び, ぴ]],
        // gu, zu, zu, bu, pu
        [ぐ, [ぐ, ず, づ, ぶ, ぷ]],
        // ge, ze, de, be, pe
        [げ, [げ, ぜ, で, べ, ぺ]],
        // go, zo, do, bo, po
        [ご, [ご, ぞ, ど, ぼ, ぽ]],
        // kya, sha, cha, hya, pya
        [きゃ, [きゃ, しゃ, ちゃ, ひゃ, ぴゃ]],
        // kyu, shu, chu, hyu, pyu
        [きゅ, [きゅ, しゅ, ちゅ, ひゅ, ぴゅ]],
        // kyo, sho, cho, hyo, pyo
        [きょ, [きょ, しょ, ちょ, ひょ, ぴょ]],
        // gya, ja, nya, bya, mya
        [ぎゃ, [ぎゃ, じゃ, にゃ, びゃ, みゃ]],
        // gya, ju, nyu, byu, my
        [ぎゅ, [ぎゅ, じゅ, にゅ, びゅ, みゅ]],
        // gyo, jo, nyo, byo, myo
        [ぎょ, [ぎょ, じょ, にょ, びょ, みょ]],
        // rya, ryu, ryu, (ja), (ju)
        [りゃ, [りゃ, りゅ, りょ, ぢゃ, ぢゅ]]
      ],
      /* eslint-enable object-property-newline */

      matchlist = kanagroups.map(([group, kanalist]) => kp_matchlist
        .creatematchdefgroup(
          group, kanalist.map(kanaitem => [kanaitem, phoneme[kanaitem]]))
      )

export default {
  kanagroups,
  matchlist,
  phoneme,
  kana
}
