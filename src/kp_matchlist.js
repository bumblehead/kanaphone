// Filename: kp_matchlist.js
// Timestamp: 2018.01.11-20:30:22 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// construct a matchlist from a kana group

import kp_queue from './kp_queue.js'

// ex,
// ['きょ', 'きょ', 'kyo', /kyo/gi]
const creatematchdef = (group, kana, phonemes) => ([
  group,
  kana,
  phonemes[0],
  new RegExp(phonemes.join('|'), 'i'),
  new RegExp(`(${phonemes.join('|')})(.*)`, 'i')
])

// ex,
// ['きょ', /kyo|sho|cho|hyo|pyo/gi, [
//   ['きょ', 'きょ', 'kyo', /kyo/gi]
//   ['きょ', 'しょ', 'sho', /sho/gi]
//   ['きょ', 'ちょ', 'cho', /cho/gi]
//   ['きょ', 'ひょ', 'hyo', /hyo/gi]
//   ['きょ', 'ぴょ', 'pyo', /pyo/gi]
// ]]
const creatematchdefgroup = (group, matchdefs) => ([
  group,
  new RegExp(matchdefs.reduce((prev, [, phonemes]) => (
    prev.concat(phonemes)
  ), []).join('|'), 'i'),
  matchdefs.map(([kana, phonemes]) => (
    creatematchdef(group, kana, phonemes)
  ))
])

const findgroupmatchdef = ([, groupre, matchdefs], str) => (
  groupre.test(str) && matchdefs.find(([, , , kanare]) => (
    kanare.test(str))))

const findrandgroupmatchdef = ([, groupre, matchdefs], str) => (
  groupre.test(str) && kp_queue
    .cyclefind(kp_queue(matchdefs), ([, , , kanare]) => (
      kanare.test(str))))

export default {
  creatematchdef,
  creatematchdefgroup,
  findgroupmatchdef,
  findrandgroupmatchdef
}
