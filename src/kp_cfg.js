// Filename: kp_cfg.js
// Timestamp: 2018.01.14-13:23:28 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const get = (spec = {}) => {
  const cfg = {}

  // should be false by default...
  cfg.iskatakana = typeof spec.iskatakana === 'boolean'
    ? spec.iskatakana : true
  cfg.ishiragana = typeof spec.ishiragana === 'boolean'
    ? spec.ishiragana : true
  cfg.chardist = 0
  cfg.lastworddist = 0
  cfg.chardistmax = typeof spec.chardistmax === 'number'
    ? spec.chardistmax : 30

  cfg.katakana_index = 0
  cfg.kanatana_index_steps = 0

  cfg.hiragana_index = 0
  cfg.hiragana_index_steps = 0

  // full signature this function,
  //   (match, substr, offset, [knagr, kana, match, offset])/3
  cfg.replace = typeof spec.replace === 'function'
    ? spec.replace
    : (word, subjstr, [, kana]) => kana

  return cfg
}

const setlastworddist = (cfg, distnum) => (
  cfg.lastworddist = distnum,
  cfg)

// total offset position...
const isreplacing = cfg => Math
  .max(cfg.lastworddist, cfg.chardist) <= 0

// move chardist away from 0
const resetchardist = cfg => (
  cfg.chardist = cfg.chardistmax,
  cfg)

const removechardist = (cfg, num) => (
  cfg.chardist = Math.max(cfg.chardist - num, 0),
  cfg.lastworddist = Math.max(cfg.lastworddist - num, 0),
  cfg)

export default Object.assign(get, {
  get,
  setlastworddist,
  isreplacing,
  resetchardist,
  removechardist
})
