import kp_matchlist from './kp_matchlist.js'
import kp_katakana from './kp_katakana.js'
import kp_hiragana from './kp_hiragana.js'
import kp_queue from './kp_queue.js'

export default (str, cfg) => {
  // rotate between between hiragana and katakana
  // cycling through the word groups in each until a match is found...
  let groupqueue
  let group
  let matchdef
  // first group has 'a' 'e' 'i' 'o' 'u'
  // avoid returning matches for them...
  const holdfirstgroup = Math.random() > 0.1 // 0.9 chance
  const isheldgroup = heldgroup => (
    holdfirstgroup && kp_queue.currenti(heldgroup) === 0)
  let groupsqueue = kp_queue([
    cfg.iskatakana && kp_katakana.matchlist,
    cfg.ishiragana && kp_hiragana.matchlist
  ].filter(e => e).map(matchgroup => (
    // construct queue around each list
    kp_queue(matchgroup)
  )))

  // cycling kata groups
  //   within kata groups cycling cub groups
  while (kp_queue.size(groupsqueue)) {
    groupqueue = kp_queue.qvalue(groupsqueue)

    if (kp_queue.iscycled(groupqueue)) {
      if (holdfirstgroup) { // try first group
        group = kp_queue.qvalue(groupqueue, 0)
        matchdef = kp_matchlist.findrandgroupmatchdef(group, str)

        if (matchdef) {
          return matchdef
        }
      }

      groupsqueue = kp_queue.rm(groupsqueue)
    } else {
      group = kp_queue.qvalue(groupqueue)
      matchdef = kp_matchlist.findrandgroupmatchdef(group, str)

      if (matchdef && !isheldgroup(groupqueue)) {
        return matchdef
      }

      groupqueue = kp_queue.next(groupqueue)
      groupsqueue = kp_queue.set(groupsqueue, groupqueue)
      groupsqueue = kp_queue.next(groupsqueue)
    }
  }
}
