import kp_matchdef from './kp_matchdef.js'

export default function kp_reduce (cfg, str, fn, prev = []) {
  const matchdef = kp_matchdef(str, cfg)
  const match = matchdef && str.match(matchdef[3])

  // recurse first and third parts
  if (match && match.index > cfg.chardistmax) {
    const bgn = str.slice(0, match.index)
    const end = str.slice(match.index + match[0].length)

    prev = kp_reduce(cfg, bgn, fn, prev)
    prev = fn(prev, match[0], matchdef)

    if (end)
      prev = kp_reduce(cfg, end, fn, prev)
  } else if (str.length) {
    prev = fn(prev, str, [])
  }

  return prev
}
