// Filename: kp_queue.js
// Timestamp: 2018.01.13-23:16:07 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const getrandomindex = arr => Math
  .floor(Math.random() * arr.length)

const set = ([starti, nexti, arr], value) => {
  arr[nexti] = value

  return [starti, nexti, arr]
}

const qnexti = (arr, i) => (
  i >= arr.length - 1 ? 0 : ++i) // eslint-disable-line no-plusplus

const create = (arr, starti) => (
  [starti, qnexti(arr, starti), arr])

const size = ([, , arr]) => arr.length

const next = ([starti, nexti, arr]) => (
  [starti, qnexti(arr, nexti), arr])

const iscycled = ([starti, nexti]) => starti === nexti

const qvalue = ([, nexti, arr], i = nexti) => arr[i]

const currenti = ([, nexti]) => nexti

const rm = ([starti, nexti, arr]) => {
  if (arr.length > 1) {
    arr.splice(nexti, 1)
  } else {
    arr = []
  }

  if (nexti >= arr.length) {
    nexti = qnexti(arr, nexti)
  }

  return [starti, nexti, arr]
}

// cycle through items until find function
// returns true
const cyclefind = (queue, fn, value) => {
  if (!iscycled(queue)) {
    value = qvalue(queue)
    if (!fn(value)) {
      queue = next(queue)
      value = cyclefind(queue, fn)
    }
  }

  return value
}

export default Object.assign((arr, starti = getrandomindex(arr)) => create(arr, starti), {
  set,
  qnexti,
  create,
  size,
  next,
  iscycled,
  qvalue,
  currenti,
  rm,
  cyclefind
})
